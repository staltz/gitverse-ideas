# Gitverse design document

First I describe use cases from the perspective of a user who doesn't care about internals. Then I describe possible implementations that provide such use cases.

All names, hashes, keys, and key lengths are hypothetical and irrelevant in these examples.

## Use cases

### Use case: Joining an existing project

You discover from somewhere that the **project** `rocket-fuel` is accessible from **project key** `8da2`. You want to read up on the project, so you open up your **Gitverse Desktop app**:

![gitverse-desktop](./gitverse.jpg)

On the left side, you press the "join project" button, input the key `8da2`, and it loads up the **project page**.

![project-page](./project-page.jpg)

The project page contains:

- List of forks
- Issues (and discussions)
- Merge requests
- List of members (everyone ever involved with the project)
- Graph (of forks)

You decide to see Alice's fork, because it seems to be what most people use. It was tagged as "init", which means this is the fork that initiated the project. There are multiple ways you could have sorted those forks:

- From most recently updated to least recently updated
- From most frequent author to least frequent author (i.e. commit contribution %)
- From most number of commits to least number of commits
- From oldest author to newest author

Clicking on Alice's fork takes you to **Fork Page** in Gitverse Desktop:

![fork-page](./fork-page.jpg)

You could also run `git clone hypergit://10ac3a0ea8100a7` and see her fork repo locally.

You go back to the project page, browse through the issues, post a comment in an issue, and then decide to make your own fork. There are two ways you can do this:

- Clicking the **Fork button** in the project page and choosing who to fork from: Alice, Bob, or Charles
- Running `$ gitverse fork hypergit://10ac3a0ea8100a7` in the terminal (forking from Alice)

Once you do that you will see "My fork" in the project page. You have full rights to push to that fork, and peers will see your fork. Eventually you create a "merge request" from your fork's `master` to Alice's `master`. Alice never commented on that merge request but Charles decided to merge your changes into his branch.

### Use case: Starting a new project

You open Gitverse Desktop, press the **new project** button on the left side, and it asks for an **existing repo**. If you don't have one, then create a new git repo:

```
echo "my new project" >> README.md
git init
git add README.md
git commit -m "first commit"
hypergit create                     # will create hypergit://8a21251a4b62f10
git push -u swarm master
```

Now you have two ways of creating a Gitverse project for this repo:

- In the desktop app, input `hypergit://8a21251a4b62f10` when creating a project
- Run `$ gitverse init hypergit://8a21251a4b62f10` or `cd myproject && gitverse init`

When using the desktop app, you will see the newly created project page, and the generated project key `3c71` which you can share with others. The new project page contains issues (zero so far), merge requests (zero), and one fork (the one you just created).

When using the terminal, the output of `gitverse init` will have shown the generated project key `3c71`. You could run `gitverse show` to read more data from the project.

### Use case: free listening

When participating in a project, you will by default automatically fetch social data (comments, issues, merge requests) from everyone involved in the project which you are (or your app is) aware of.

You have the option of stop listening to any other member, which will prevent their data from being fetched in the future, and will hide (and/or delete) their past data from your computer. You also have the option of stop listening to that member in all other projects you are involved in.

### Use case: hard forks

If my fork diverges from Alice's fork considerable, and/or if the community itself has a partition due to disagreement, there must be a way of preserving the same fork repo, but starting a new project for that fork. In other words, if I'm working on project `8da2`, I may choose to hard fork it, keeping the git repo as is, but creating a new project `90e0`, with zero issues and zero merge requests.

## Technical requirements

- Project data is only fetched for projects I have joined
  - Project data consists of: comments, issues, merge requests, list of forks
  - This is necessary when aiming for a huge scale of open source projects
- Project data is distributed and has no single point of acqui$ition
- Unforgeable replication of append-only feeds in project data
- My membership in a project is tied to my Gitverse identity/account
  - This is desirable for simple reusability of identity when joining a new project
  - This is desirable for content discoverability: know when my friend joined another project
  - This is necessary for cross-project "blocking" and free listening

## Technical design – plan A

The basic setup is:

- Git lives in hypergit (blue)
- Project related data lives in a dedicated SSB feed (purple) related to your main SSB feed (green)
- Connections with peole interested in the same project happen through a DHT (for strangers who know the project key) or through known Pubs (for your friends who want to find your projects)

![Plan A](./plan-a.jpg)

More details:

- Your project fork is a hypergit repo (blue in the diagram)
- The SSB feed used in a project is specific for that project, let's call it your **project account**
- The project account is a "subaccount" of your main SSB feed
- Project accounts are implemented with sameAs (or some variant of sameAs)
- A project account is initially disconnected from everything else, it starts absolutely empty (no gossip.json, no friendships)
- The project key is (or is derived from) the SHA-1 of the initial commit of the repo
  - This way anyone who locally has the git repo can lookup the community for this repo

About the Git setup:

Using `git config user.ssb` we can set the project account ID (I'm not sure yet how to do this in git config, probably something more needs to be done) so that each git commit also points to the project account SSB id. This is important so that people who join the project know (with guarantees) who started the project.

About the SSB setup:

- We need to create "partitions" in the scuttleverse
- People replicating in **projectspace (purple)** should not know of people in **friendspace (green)**
- One way (not the only way) of creating partitions is through altnets
- Another way of creating partitions is by not sharing gossip configs between projectspace and friendspace
  - Unlike altnets, this would enable potentially merging two projectspaces together later in the future. I can't think of why one would need this, but it would be possible
- The DHT is used to connect with strangers who are interested in the project
- SSB was chosen for the projectspace instead of Hyperdb because in projectspace you want to guarantee append-preserving orders of the social feeds, otherwise critical project communication could be tampered simply by omitting some messages
- A friend from friendspace can go into your projectspace
  - The `sameAs` written in your main SSB feed can have a pointer to the project key
- A person in projectspace cannot go into your friendspace
  - Unless you explicitly give them user invites into friendspace
  - They only know your friendspace SSB id
  - They need to know your friendspace SSB id in case they want to block your friendspace ID and all subaccounts in all projectspaces
  - This is why we need to copy-paste your latest friendspace `about` message into your projectspace feed

How a typical projectspace feed would look like:

1. (beginning of the SSB feed)
2. `about` message
  - `name: "Alice"`
  - `image: avatarblob`
  - `fromId: "@yourFriendspaceId"`
3. `fork` message
  - `repo: "hypergit://a010101b010`
4. `git` message (for each git commit in *your* fork)
  - `commit`: the SHA-1 for the commit
  - no other git data required
  - useful for gathering metadata when listing the forks in the project
5. `issue` message
  - like patchcore posts
6. `pull-request` message

## Comments (from staltz)

**Independence from hypergit** It's interesting that hypergit is not *required* at any point. We could have hosted the git repo anywhere we wish, *including* GitHub. In Gitverse, the list of forks could list both hypergit repos as well as GitLab, GitHub, git-ssb (!) repos. This is because git itself is already easily transferable between one remote type to another. This is also cool because we get easy interop between all these different git hosts. Hence Gitverse is all about the community part (issues, PRs, etc) and allows interchangeable hosting for the repos.

**Unsure about extending git** It would be nice if we could replace GPG signing in git, with signatures from the ed5519 keys in SSB. Also would be nice to replace `git config user.email` with `user.ssb` if possible. But I'm not sure if Git is open enough for these kinds of extensions.

**Unsure about Pubs dedicated to projects** It would be nice if one pub could host data from friendspace and from projectspaces (of multiple projects) **without** mixing those spaces together. I'm not sure how well can scuttlebot currently support multiple owner feeds sharing the same sbot process. This is probably one grant project worth of work to do. Nevertheless it's useful overall, also beyond Gitverse use cases.
